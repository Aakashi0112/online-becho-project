@if($setting['view-method'] =='native')
<div class="card">
	<div class="card-body">	
		<div class="toolbar-nav">
			<div class="row">
				<div class="col-md-6" >
					<a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm btn-danger  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>	
				</div>
				<div class="col-md-6 text-right " >
					<div class="btn-group">
						<a href="{{ ($prevnext['prev'] != '' ? url('inventory/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-primary btn-sm" onclick="ajaxViewDetail('#inventory',this.href); return false; "><i class="fa fa-arrow-left"></i>  </a>	
						<a href="{{ ($prevnext['next'] != '' ? url('inventory/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-primary  btn-sm " onclick="ajaxViewDetail('#inventory',this.href); return false; "> <i class="fa fa-arrow-right"></i>  </a>
			   		</div>			
				</div>	

				
			</div>
		</div>	
		
@endif	

		<table class="table  table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Snapshot-date', (isset($fields['snapshot-date']['language'])? $fields['snapshot-date']['language'] : array())) }}</td>
						<td>{{ $row->snapshot-date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sku', (isset($fields['sku']['language'])? $fields['sku']['language'] : array())) }}</td>
						<td>{{ $row->sku}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Fnsku', (isset($fields['fnsku']['language'])? $fields['fnsku']['language'] : array())) }}</td>
						<td>{{ $row->fnsku}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Asin', (isset($fields['asin']['language'])? $fields['asin']['language'] : array())) }}</td>
						<td>{{ $row->asin}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Product-name', (isset($fields['product-name']['language'])? $fields['product-name']['language'] : array())) }}</td>
						<td>{{ $row->product-name}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Condition', (isset($fields['condition']['language'])? $fields['condition']['language'] : array())) }}</td>
						<td>{{ $row->condition}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Available', (isset($fields['available']['language'])? $fields['available']['language'] : array())) }}</td>
						<td>{{ $row->available}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Pending-removal-quantity', (isset($fields['pending-removal-quantity']['language'])? $fields['pending-removal-quantity']['language'] : array())) }}</td>
						<td>{{ $row->pending-removal-quantity}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-0-to-90-days', (isset($fields['inv-age-0-to-90-days']['language'])? $fields['inv-age-0-to-90-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-0-to-90-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-91-to-180-days', (isset($fields['inv-age-91-to-180-days']['language'])? $fields['inv-age-91-to-180-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-91-to-180-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-181-to-270-days', (isset($fields['inv-age-181-to-270-days']['language'])? $fields['inv-age-181-to-270-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-181-to-270-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-271-to-365-days', (isset($fields['inv-age-271-to-365-days']['language'])? $fields['inv-age-271-to-365-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-271-to-365-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-365-plus-days', (isset($fields['inv-age-365-plus-days']['language'])? $fields['inv-age-365-plus-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-365-plus-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Currency', (isset($fields['currency']['language'])? $fields['currency']['language'] : array())) }}</td>
						<td>{{ $row->currency}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Qty-to-be-charged-ltsf-5-mo', (isset($fields['qty-to-be-charged-ltsf-5-mo']['language'])? $fields['qty-to-be-charged-ltsf-5-mo']['language'] : array())) }}</td>
						<td>{{ $row->qty-to-be-charged-ltsf-5-mo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Projected-ltsf-5-mo', (isset($fields['projected-ltsf-5-mo']['language'])? $fields['projected-ltsf-5-mo']['language'] : array())) }}</td>
						<td>{{ $row->projected-ltsf-5-mo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Qty-to-be-charged-ltsf-12-mo', (isset($fields['qty-to-be-charged-ltsf-12-mo']['language'])? $fields['qty-to-be-charged-ltsf-12-mo']['language'] : array())) }}</td>
						<td>{{ $row->qty-to-be-charged-ltsf-12-mo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estimated-ltsf-next-charge', (isset($fields['estimated-ltsf-next-charge']['language'])? $fields['estimated-ltsf-next-charge']['language'] : array())) }}</td>
						<td>{{ $row->estimated-ltsf-next-charge}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Units-shipped-t7', (isset($fields['units-shipped-t7']['language'])? $fields['units-shipped-t7']['language'] : array())) }}</td>
						<td>{{ $row->units-shipped-t7}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Units-shipped-t30', (isset($fields['units-shipped-t30']['language'])? $fields['units-shipped-t30']['language'] : array())) }}</td>
						<td>{{ $row->units-shipped-t30}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Units-shipped-t60', (isset($fields['units-shipped-t60']['language'])? $fields['units-shipped-t60']['language'] : array())) }}</td>
						<td>{{ $row->units-shipped-t60}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Units-shipped-t90', (isset($fields['units-shipped-t90']['language'])? $fields['units-shipped-t90']['language'] : array())) }}</td>
						<td>{{ $row->units-shipped-t90}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alert', (isset($fields['alert']['language'])? $fields['alert']['language'] : array())) }}</td>
						<td>{{ $row->alert}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Your-price', (isset($fields['your-price']['language'])? $fields['your-price']['language'] : array())) }}</td>
						<td>{{ $row->your-price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sales-price', (isset($fields['sales-price']['language'])? $fields['sales-price']['language'] : array())) }}</td>
						<td>{{ $row->sales-price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Lowest-price-new-plus-shipping', (isset($fields['lowest-price-new-plus-shipping']['language'])? $fields['lowest-price-new-plus-shipping']['language'] : array())) }}</td>
						<td>{{ $row->lowest-price-new-plus-shipping}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Lowest-price-used', (isset($fields['lowest-price-used']['language'])? $fields['lowest-price-used']['language'] : array())) }}</td>
						<td>{{ $row->lowest-price-used}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Recommended-action', (isset($fields['recommended-action']['language'])? $fields['recommended-action']['language'] : array())) }}</td>
						<td>{{ $row->recommended-action}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Healthy-inventory-level', (isset($fields['healthy-inventory-level']['language'])? $fields['healthy-inventory-level']['language'] : array())) }}</td>
						<td>{{ $row->healthy-inventory-level}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Recommended-sales-price', (isset($fields['recommended-sales-price']['language'])? $fields['recommended-sales-price']['language'] : array())) }}</td>
						<td>{{ $row->recommended-sales-price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Recommended-sale-duration-days', (isset($fields['recommended-sale-duration-days']['language'])? $fields['recommended-sale-duration-days']['language'] : array())) }}</td>
						<td>{{ $row->recommended-sale-duration-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Recommended-removal-quantity', (isset($fields['recommended-removal-quantity']['language'])? $fields['recommended-removal-quantity']['language'] : array())) }}</td>
						<td>{{ $row->recommended-removal-quantity}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estimated-cost-savings-of-recommended-actions', (isset($fields['estimated-cost-savings-of-recommended-actions']['language'])? $fields['estimated-cost-savings-of-recommended-actions']['language'] : array())) }}</td>
						<td>{{ $row->estimated-cost-savings-of-recommended-actions}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sell-through', (isset($fields['sell-through']['language'])? $fields['sell-through']['language'] : array())) }}</td>
						<td>{{ $row->sell-through}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Item-volume', (isset($fields['item-volume']['language'])? $fields['item-volume']['language'] : array())) }}</td>
						<td>{{ $row->item-volume}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Volume-unit-measurement', (isset($fields['volume-unit-measurement']['language'])? $fields['volume-unit-measurement']['language'] : array())) }}</td>
						<td>{{ $row->volume-unit-measurement}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Storage-type', (isset($fields['storage-type']['language'])? $fields['storage-type']['language'] : array())) }}</td>
						<td>{{ $row->storage-type}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Storage-volume', (isset($fields['storage-volume']['language'])? $fields['storage-volume']['language'] : array())) }}</td>
						<td>{{ $row->storage-volume}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Marketplace', (isset($fields['marketplace']['language'])? $fields['marketplace']['language'] : array())) }}</td>
						<td>{{ $row->marketplace}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Product-group', (isset($fields['product-group']['language'])? $fields['product-group']['language'] : array())) }}</td>
						<td>{{ $row->product-group}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sales-rank', (isset($fields['sales-rank']['language'])? $fields['sales-rank']['language'] : array())) }}</td>
						<td>{{ $row->sales-rank}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Days-of-supply', (isset($fields['days-of-supply']['language'])? $fields['days-of-supply']['language'] : array())) }}</td>
						<td>{{ $row->days-of-supply}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estimated-excess-quantity', (isset($fields['estimated-excess-quantity']['language'])? $fields['estimated-excess-quantity']['language'] : array())) }}</td>
						<td>{{ $row->estimated-excess-quantity}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Weeks-of-cover-t30', (isset($fields['weeks-of-cover-t30']['language'])? $fields['weeks-of-cover-t30']['language'] : array())) }}</td>
						<td>{{ $row->weeks-of-cover-t30}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Weeks-of-cover-t90', (isset($fields['weeks-of-cover-t90']['language'])? $fields['weeks-of-cover-t90']['language'] : array())) }}</td>
						<td>{{ $row->weeks-of-cover-t90}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Featuredoffer-price', (isset($fields['featuredoffer-price']['language'])? $fields['featuredoffer-price']['language'] : array())) }}</td>
						<td>{{ $row->featuredoffer-price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sales-shipped-last-7-days', (isset($fields['sales-shipped-last-7-days']['language'])? $fields['sales-shipped-last-7-days']['language'] : array())) }}</td>
						<td>{{ $row->sales-shipped-last-7-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sales-shipped-last-30-days', (isset($fields['sales-shipped-last-30-days']['language'])? $fields['sales-shipped-last-30-days']['language'] : array())) }}</td>
						<td>{{ $row->sales-shipped-last-30-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sales-shipped-last-60-days', (isset($fields['sales-shipped-last-60-days']['language'])? $fields['sales-shipped-last-60-days']['language'] : array())) }}</td>
						<td>{{ $row->sales-shipped-last-60-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sales-shipped-last-90-days', (isset($fields['sales-shipped-last-90-days']['language'])? $fields['sales-shipped-last-90-days']['language'] : array())) }}</td>
						<td>{{ $row->sales-shipped-last-90-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-0-to-30-days', (isset($fields['inv-age-0-to-30-days']['language'])? $fields['inv-age-0-to-30-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-0-to-30-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-31-to-60-days', (isset($fields['inv-age-31-to-60-days']['language'])? $fields['inv-age-31-to-60-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-31-to-60-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-61-to-90-days', (isset($fields['inv-age-61-to-90-days']['language'])? $fields['inv-age-61-to-90-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-61-to-90-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-91-to-150-days', (isset($fields['inv-age-91-to-150-days']['language'])? $fields['inv-age-91-to-150-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-91-to-150-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-151-to-180-days', (isset($fields['inv-age-151-to-180-days']['language'])? $fields['inv-age-151-to-180-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-151-to-180-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-181-to-330-days', (isset($fields['inv-age-181-to-330-days']['language'])? $fields['inv-age-181-to-330-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-181-to-330-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inv-age-331-to-365-days', (isset($fields['inv-age-331-to-365-days']['language'])? $fields['inv-age-331-to-365-days']['language'] : array())) }}</td>
						<td>{{ $row->inv-age-331-to-365-days}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Estimated-storage-cost-next-month', (isset($fields['estimated-storage-cost-next-month']['language'])? $fields['estimated-storage-cost-next-month']['language'] : array())) }}</td>
						<td>{{ $row->estimated-storage-cost-next-month}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inbound-quantity', (isset($fields['inbound-quantity']['language'])? $fields['inbound-quantity']['language'] : array())) }}</td>
						<td>{{ $row->inbound-quantity}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inbound-working', (isset($fields['inbound-working']['language'])? $fields['inbound-working']['language'] : array())) }}</td>
						<td>{{ $row->inbound-working}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inbound-shipped', (isset($fields['inbound-shipped']['language'])? $fields['inbound-shipped']['language'] : array())) }}</td>
						<td>{{ $row->inbound-shipped}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Inbound-received', (isset($fields['inbound-received']['language'])? $fields['inbound-received']['language'] : array())) }}</td>
						<td>{{ $row->inbound-received}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('No-sale-last-6-months', (isset($fields['no-sale-last-6-months']['language'])? $fields['no-sale-last-6-months']['language'] : array())) }}</td>
						<td>{{ $row->no-sale-last-6-months}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Reserved-quantity', (isset($fields['reserved-quantity']['language'])? $fields['reserved-quantity']['language'] : array())) }}</td>
						<td>{{ $row->reserved-quantity}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Unfulfillable-quantity', (isset($fields['unfulfillable-quantity']['language'])? $fields['unfulfillable-quantity']['language'] : array())) }}</td>
						<td>{{ $row->unfulfillable-quantity}} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	
		 
@if($setting['view-method'] =='native')
	</div>
</div>
@endif		