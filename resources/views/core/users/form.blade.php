@extends('layouts.app')

@section('content')
<div class="page-titles">
  <h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
</div>
<div  class="card">
	<div class="card-body">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>	

		{!! Form::open(array('url'=>'core/users?return='.$return, 'class'=>'form-vertical sximo-form validated','files' => true )) !!}
		<div class="row">
		<div class="col-md-6">
			<fieldset>
				<legend> Account Credit </legend>
					
					
				  <div class="form-group hidethis " style="display:none;">
					<label for="Id" class=" control-label "> Id </label>
					<div class="">
					  {!! Form::text('id', $row['id'],array('class'=>'form-control form-control-sm', 'placeholder'=>'',   )) !!} 
					 </div> 
					 
				  </div> 					
				  <div class="form-group  " >
					<label for="Group / Level" class=" control-label "> Group / Level <span class="asterix"> * </span></label>
					<div class="">
					  <select name='group_id' rows='5' id='group_id' code='{$group_id}' class='select2 form-control  form-control-sm'  required  ></select> 
					 </div> 
					
				  </div> 					
					<div class="row">
						<div class="col-md-6">
							  <div class="form-group  " >
								<label for="First Name" class=" control-label "> First Name <span class="asterix"> * </span></label>
								<div class="">
								  {!! Form::text('first_name', $row['first_name'],array('class'=>'form-control  form-control-sm', 'placeholder'=>'', 'required'=>'true'  )) !!} 
								 </div> 
								
							  </div> 
						</div>	  	
						<div class="col-md-6">				
							  <div class="form-group  " >
								<label for="Last Name" class=" control-label "> Last Name </label>
								<div class="">
								  {!! Form::text('last_name', $row['last_name'],array('class'=>'form-control  form-control-sm', 'placeholder'=>'',   )) !!} 
								 </div> 
								 <div class="col-md-2">
								 	
								 </div>
							  </div> 	
						</div>
					</div>		  				
				  <div class="form-group  " >
					<label for="Email" class=" control-label "> Email <span class="asterix"> * </span></label>
					<div class="">
					  {!! Form::text('email', $row['email'],array('class'=>'form-control  form-control-sm', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'email'   )) !!} 
					 </div> 
					 
				  </div> 		
				  
				  <div class="form-group  " >
					<label for="Company Access" class=" control-label "> Company Access <span class="asterix"> * </span></label>
					<div class="">
					  <select multiple name='allowed_company[]' rows='5' id='allowed_company' code='{$allowed_company}' class='select2 form-control  form-control-sm'  required  ></select> 
					 </div> 
					
				  </div> 

				  <div class="form-group  " >
					<label for="Default Company" class=" control-label "> Default Company<span class="asterix"> * </span></label>
					<div class="">
					  <select name='default_company' rows='5' id='default_company' code='{$default_company}' class='select2 form-control  form-control-sm'  required  ></select> 
					 </div> 
					
				  </div>
	
				  <div class="form-group  " >
					<label for="Status" class=" control-label "> Status <span class="asterix"> * </span></label>
					<div class="">
						<input type='radio' name='active' value ='1' required @if($row['active'] == '1') checked="checked" @endif class="filled-in" id="Active">  <label for="Active"> Active </label>  

						<input type='radio' name='active' value ='0' required @if($row['active'] == '0') checked="checked" @endif class="filled-in" id="Inactive">  <label for="Inactive"> Inactive </label> 

						<input type='radio' name='active' value ='2' required @if($row['active'] == '2') checked="checked" @endif class="filled-in" id="Banned">  <label for="Banned"> Banned </label>  
					 </div> 
					 
				  </div> 


				  <div class="form-group  " >
					<label for="Avatar" class=" control-label "> Avatar </label>
					<div class="">
					  <input  type='file' name='avatar' id='avatar' @if($row['avatar'] =='') class='required' @endif style='width:150px !important;'  />
						 	<div >
							{!! SiteHelpers::showUploadedFile($row['avatar'],'/uploads/users/') !!}
							
					</div>					
 
					 </div> 
					 
				  </div> 


	  			

			</div>

			
		</fieldset>

		<div class="col-md-6">	  
			<fieldset>
				<legend> Password </legend>
				<p>
					@if($row['id'] !='')
							{{ Lang::get('core.notepassword') }}
						@else
							Create Password
						@endif	 
				</p>		


				<div class="form-group">
					<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.newpassword') }} </label>
					<div class="col-md-8">
					<input name="password" type="password" id="password" class="form-control form-control-sm" value=""
					@if($row['id'] =='')
						required
					@endif
					 /> 
					 </div> 
				</div>  
				  
				  <div class="form-group">
					<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.conewpassword') }} </label>
					<div class="col-md-8">
					<input name="password_confirmation" type="password" id="password_confirmation" class="form-control form-control-sm" value=""
					@if($row['id'] =='')
						required
					@endif		
					 />  
					 </div> 
				  </div>  				  
				  
			</fieldset>
			<fieldset>
				<legend> </legend>
				<div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
						<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
						<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
						<button type="button" onclick="location.href='{{ URL::to('core/users?return='.$return) }}' " class="btn btn-warning btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div>

			</fieldset>	
		 
		 </div>			
			
			
			<div style="clear:both"></div>	
				
					
			> 
		 <input type="hidden" name="action_task" value="save" />
	</div>
	{!! Form::close() !!}
	</div>
</div>
		 
	<script>

		$("#group_id").change( function() {
			var g_id = $("#group_id").val();
			let newOption = new Option('ALL','all');
			const select = document.querySelector('#allowed_company'); 
			
			if(g_id == 1){
				select.add(newOption);
				document.getElementById("default_company").required = false;
			}
			else{
				document.getElementById("default_company").required = true;
				let index = select.options.length;
				//alert(index);
				select.remove(--index);
			}
		});

		$("#allowed_company").change( function (){
			var allowed_company = $("#allowed_company").val();
			//var arr = allowed_company.split(",");
			//alert(allowed_company[1]);
			var ddlArray= new Array();
			const default_company1 = document.querySelector('#default_company');
			var default_company = document.getElementById('default_company');
			
			for (i = 1; i < default_company.options.length; i++) {
				ddlArray[i] = default_company .options[i].value;
				alert(ddlArray[i]);
			}

			for (var i = 0; i < ddlArray.length; i++) { 
				for (var j = 0; j < allowed_company.length; j++) { 
					if (ddlArray[i] !== allowed_company[j]) {
						default_company1.remove(j+1);
					}
				}
    		}

			// var allowed_company = document.querySelector("#allowed_company");
			// const default_company = document.querySelector('#default_company');

			// let selected = [];

			// for (let i = 0; i < allowed_company.options.length; i++) {
			// 	selected[i] = allowed_company.options[i].selected;
			// 	//alert(selected[i]);
			// 	if(selected[i]){
			// 		var value = document.querySelector("#allowed_company").selectedIndex;
			// 		var text = allowed_company.options[allowed_company.selectedIndex].text;
			// 		let newOption = new Option(text,value);
			// 		default_company.add(newOption);
			// 	}
			// }

			var value = document.querySelector("#allowed_company").selectedIndex;
			//var value = allowed_company.options[allowed_company.selectedIndex].val;
			var text = allowed_company.options[allowed_company.selectedIndex].text;
			alert(value + text);
			// dvlaue.push(value);
			// dtext.push(text);
			//debugger;	
			const default_company = document.querySelector('#default_company'); 
			
			//if(default_company.options.value != value){
				let newOption = new Option(text,value);
				default_company.add(newOption);
			//}
			
			
			// 
			// for(var i=0; i<default_company.length; i++){
			// 	if(allowed_company.indexOf(i) == -1){
			// 		default_company.remove(i);
			// 	}
			// }

			// $("#default_company option").each(function() {
			// 	//alert($(this).val());
			// 	var index = allowed_company.indexOf($(this).val());
			// 	//alert(index);
			// 	$("#container").append(this.value + ' ');        // or $(this).val()
			// 	if(allowed_company.indexOf($(this).val()) == "-1"){
			// 		default_company.remove($(this).val());
			// 	}
			// });
			
			
		});
	</script> 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		$("#group_id").jCombo("{{ URL::to('core/users/comboselect?filter=tb_groups:group_id:name') }}",
		{  selected_value : '{{ $row["group_id"] }}' });

		$("#allowed_company").jCombo("{{ URL::to('core/users/comboselect?filter=companies:id:name') }}",
		{  selected_value : '{{ $row["id"] }}' });

		$("#default_company").jCombo("{{ URL::to('core/users/comboselect?filter=companies:id:name') }}",
		{  selected_value : '{{ $row["id"] }}' });
		 
	});
	
	</script>		
	
@stop