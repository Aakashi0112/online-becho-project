<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class classgroup extends Sximo  {
	
	protected $table = 'grouptable';
	protected $primaryKey = 'ID';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT grouptable.* FROM grouptable  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE grouptable.ID IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
