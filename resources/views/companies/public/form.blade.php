

		 {!! Form::open(array('url'=>'companies/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> companies</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Name" class=" control-label col-md-4 "> Name </label>
										<div class="col-md-8">
										  <input  type='text' name='name' id='name' value='{{ $row['name'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Address" class=" control-label col-md-4 "> Address </label>
										<div class="col-md-8">
										  <textarea name='address' rows='5' id='address' class='form-control form-control-sm '  
				           >{{ $row['address'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="City" class=" control-label col-md-4 "> City </label>
										<div class="col-md-8">
										  <input  type='text' name='city' id='city' value='{{ $row['city'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Zip code" class=" control-label col-md-4 "> Zip code </label>
										<div class="col-md-8">
										  <input  type='text' name='zip' id='zip' value='{{ $row['zip'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Country" class=" control-label col-md-4 "> Country </label>
										<div class="col-md-8">
										  <input  type='text' name='country' id='country' value='{{ $row['country'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Contact Person" class=" control-label col-md-4 "> Contact Person </label>
										<div class="col-md-8">
										  <input  type='text' name='contact_person' id='contact_person' value='{{ $row['contact_person'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Contact Number" class=" control-label col-md-4 "> Contact Number </label>
										<div class="col-md-8">
										  <input  type='text' name='contact_number' id='contact_number' value='{{ $row['contact_number'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Email Address" class=" control-label col-md-4 "> Email Address </label>
										<div class="col-md-8">
										  <input  type='text' name='email' id='email' value='{{ $row['email'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Marketplace Id" class=" control-label col-md-4 "> Marketplace Id </label>
										<div class="col-md-8">
										  <select name='marketplace_id[]' multiple rows='5' id='marketplace_id' class='select2 '   ></select> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Url" class=" control-label col-md-4 "> Url </label>
										<div class="col-md-8">
										  <input  type='text' name='url' id='url' value='{{ $row['url'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Staus" class=" control-label col-md-4 "> Staus </label>
										<div class="col-md-8">
										  
					<?php $staus = explode(',',$row['staus']);
					$staus_opt = array( 'Active' => 'Active' ,  'Inactive' => 'Inactive' , ); ?>
					<select name='staus' rows='5'   class='select2 '  > 
						<?php 
						foreach($staus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['staus'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 
									  </div> {!! Form::hidden('modified', $row['modified']) !!}</fieldset></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#marketplace_id").jCombo("{!! url('companies/comboselect?filter=marketplace:id:name') !!}",
		{  selected_value : '{{ $row["marketplace_id"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
