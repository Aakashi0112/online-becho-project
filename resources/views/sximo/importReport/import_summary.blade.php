@extends('layouts.app')

@section('content')

<div class="page-titles">
  <h2> {{ $pageTitle }}  <small>  {{ $pageNote }} </small> </h2>
</div>

<?php if(count($errors) > 0) { ?> 
<div class="alert alert-danger errorMessage">
    <button type="button" class="close error" aria-label="Close"><span aria-hidden="true">×</span></button>
    <strong>
        <p style="">Total <?php echo count($errors) ?> records were not inserted or updated!!</p>
    </strong>
    <?php foreach($errors as $key => $val) {?>
        <p>The following errors occurs in Line <?php echo $key;?></p>
        <ul>
            @foreach($val->all() as $e) 
                <li>{{ $e }}</li>
            @endforeach
        </ul>
    <?php } ?>
</div> 
<?php } ?>

@if(count($insertion) > 0 || count($updation) > 0)
<div class="text-center result">
    <p class="alert alert-success"><?php echo $successMessage; ?>
        <button type="button" class="close success" aria-label="Close"><span aria-hidden="true">×</span></button>
    </p>
</div>
@endif

<div>
    <strong>Data Insertion Detail</strong>
</div>

<center>

<table class="table table-hover " id="table" style="width:90% !important;">
    <thead class="no-border">
    <tr>
        <th class="thead" field="name1">No.</th>
        <th class="thead" field="name2">Product SKU</th>
        <th class="thead" field="name3">Product Name</th
    </tr>
    </thead>

    <tbody>
        <?php if(count($insertion) > 0) { 
            $num = 1;
            foreach($insertion as $i) { 
        ?>
            <tr>
                <td><?php echo $num; ?></td>
                <td><?php echo $i['sku']; ?></td>
                <td><?php echo $i['product_name']; ?></td>
            </tr>
        <?php 
           $num++; }
        } else{
            echo "<tr><td colspan='3' class='text-center'>No Records were inserted</td></tr>";
        }
        ?>
    </tbody>

</table>
</center>

<br /><br />
<div>
    <strong>Data Updation Detail</strong>
</div>

<center>

<table class="table table-hover " id="table" style="width:90% !important;">
    <thead class="no-border">
    <tr>
        <th class="thead" field="name1">No.</th>
        <th class="thead" field="name2">Product SKU</th>
        <th class="thead" field="name3">Product Name</th>
    </tr>
    </thead>

    <tbody>
        <?php if(count($updation) > 0) { 
            $num = 1;
            foreach($updation as $i) { 
        ?>
            <tr>
                <td><?php echo $num; ?></td>
                <td><?php echo $i['sku']; ?></td>
                <td><?php echo $i['product_name']; ?></td>
            </tr>
        <?php 
           $num++;}
        } else{
            echo "<tr><td colspan='3' class='text-center'>No Records were updated</td></tr>";
        }
        ?>
    </tbody>

</table>
</center>

<script>
    $('.success').click(function(){
        $('.result').hide();
    });

    $('.error').click(function(){
        $('.errorMessage').hide();
    });
</script>
@stop