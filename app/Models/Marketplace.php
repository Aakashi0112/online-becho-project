<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class marketplace extends Sximo  {
	
	protected $table = 'marketplace';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT marketplace.* FROM marketplace  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE marketplace.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
