<?php
        
// Start Routes for fullcrud 
Route::resource('fullcrud','FullcrudController');
// End Routes for fullcrud 

                    
// Start Routes for formdefault 
Route::resource('formdefault','FormdefaultController');
// End Routes for formdefault 

                    
// Start Routes for formgroupped 
Route::resource('formgroupped','FormgrouppedController');
// End Routes for formgroupped 

                    
// Start Routes for formwizard 
Route::resource('formwizard','FormwizardController');
// End Routes for formwizard 

                    
// Start Routes for formtab 
Route::resource('formtab','FormtabController');
// End Routes for formtab 

                    
// Start Routes for sxtask 
Route::resource('sxtask','SxtaskController');
// End Routes for sxtask 

                    
// Start Routes for classgroup 
Route::resource('classgroup','ClassgroupController');
// End Routes for classgroup 

                    
// Start Routes for classgroup 
Route::resource('classgroup','ClassgroupController');
// End Routes for classgroup 

                    
// Start Routes for userdatatestcontroller 
Route::resource('userdatatestcontroller','UserdatatestcontrollerController');
// End Routes for userdatatestcontroller 

                    
// Start Routes for userdatatesttwo 
Route::resource('userdatatesttwo','UserdatatesttwoController');
// End Routes for userdatatesttwo 

                    
// Start Routes for userdatatestthree 
Route::resource('userdatatestthree','UserdatatestthreeController');
// End Routes for userdatatestthree 

                    
// Start Routes for companies 
Route::resource('companies','CompaniesController');
// End Routes for companies 

                    
// Start Routes for systemusers 
Route::resource('systemusers','SystemusersController');
// End Routes for systemusers 

                    
// Start Routes for marketplace 
Route::resource('marketplace','MarketplaceController');
// End Routes for marketplace 

                    
// Start Routes for inventory 
Route::resource('inventory','InventoryController');
// End Routes for inventory 

                    
// Start Routes for events 
Route::resource('events','EventsController');
// End Routes for events 

                    ?>