<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class inventory extends Sximo  {
	
	protected $table = 'inventory';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return " SELECT inventory.* FROM inventory ";
	}	

	public static function queryWhere(  ){
		
		return " WHERE inventory.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
