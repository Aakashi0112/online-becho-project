<?php namespace App\Http\Controllers\sximo;
use App\Models\Sximo\Module;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CsvData;
use Validator, Input, Redirect; 
use Illuminate\Support\Facades\Auth;

class ImportController extends Controller
{
    public function __construct()
	{
		parent::__construct();
        $this->middleware(function ($request, $next) {        
            // if(!session()->has('company')){
            //     return redirect('user/login')->with('msgstatus', 'error')->with('messagetext','You are not login'); 
            // }
        
            // if(session('gid') !='1')
            //     return redirect('dashboard')
            //     ->with('message','You Dont Have Access to Page !')->with('status','error');            
            return $next($request);
        });

        $driver             = config('database.default');
        $database           = config('database.connections');
       
        $this->db           = $database[$driver]['database'];
        $this->dbuser       = $database[$driver]['username'];
        $this->dbpass       = $database[$driver]['password'];
        $this->dbhost       = $database[$driver]['host']; 
		$this->model = new Module();

        $this->data = array_merge(array(
            'pageTitle' =>  'Report',
            'pageNote'  =>  'Import Your Report',
            
        ),$this->data)  ;

        //$vrules = array();
    }
    public function getImportReport()
    {
        $company_id = base64_decode(session('company'));
        $row = \DB::select("SELECT * FROM companies WHERE id = $company_id");
        $marketplace_id = explode(",",$row[0]->marketplace_id);
        foreach($marketplace_id as $id){
            $this->data['marketplaces'][] = \DB::table('marketplace')->get()->where('id',$id);
        }
        return view('sximo.importReport.import',$this->data);
    }

    public function parseImport(Request $request)
    { 
        $rules = array(
			'marketplace'=>'required',
			'report'=>'required',
			'fileimport'=>'required'
		);
        $validator = $this->validateData($request->all(), $rules);
        if($validator->passes())
        {
            $file = 	$request->file('fileimport');
            $filename = $file->getClientOriginalName();
            $mimes = array('text/csv');
            if(!in_array($_FILES['fileimport']['type'],$mimes)){
                return redirect()->back()->with(['status'=>'error','message'=>'The fileimport must be a file of type: csv. !']);
            }
            
            $uploadSuccess = $file->move('./uploads/' , $filename );
            
            $table = $request->input('report');
            $marketplace = $request->input('marketplace');

            $this->data['data'] = $this->get_csvFields_mapping_data($table, $marketplace);
            $this->data['table'] = $table;
            $this->data['marketplace'] = $request->input('marketplace');
            $this->data['fileName'] = $filename;
            return view('sximo.importReport.import_fields', $this->data);
        }
        else
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }
    
    public function processImport( Request $request )
    {
        $insert = array();
        $update = array();
        $marketplace = $request->input('marketplace');
        $table = $request->input('table');

        $filename = $request->input('fileName');
        $csv = $this->getFileData($filename);
        $csv_file_fields = $this->replace_fields_space_with_underScore($csv[0]);

        $is_not_import = $request->input('is_not_import');
        $data = $this->get_csvFields_mapping_data($table, $marketplace);
        $fields = $this->get_fields_to_import($data, $is_not_import);
        
        foreach($csv_file_fields as $key => $val){
            if(!in_array($val, $fields)){
                for($i=0; $i< count($csv); $i++)
                    unset($csv[$i][$key]);              // To unset fields value which is checked under do not import
            }
        }

        $validationRules = $this->get_validation_rules($data);
        
        unset($csv[0]);
        $errors = array();
        $rowCount = 1;
        foreach($csv as $row){ 
            $data = array();
            $skuCount = 0;
            $row = array_values($row);
           
            foreach($fields as $key=>$val)
            {
                if($fields[$key] == "sku" || $fields[$key] == "SKU"){
                    $skuCount = $this->validateSku($table,$row[$key],$marketplace);
                }
                $data[$val] = $this->convert_row_to_data_obj($row[$key]);
                //$data[$val] = (isset($row[$key]) ? $row[$key] : '' ) ;	
                
            }
            $data['marketplace_id'] = $marketplace;
            $data['company_id'] = base64_decode(session('company'));
            $validator = $this->validateData($data, $validationRules);
    
            if($validator->passes()){
                $details = array(
                    'sku' => (
                        (isset($data['sku'])) ?  $data['sku'] :
                       ((isset($data['SKU'])) ? $data['SKU'] : $data['Sku_No'])
                    ),
                    'product_name' => (
                        (isset($data['product_name'])) ?  $data['product_name'] :
                       ((isset($data['Title'])) ? $data['Title'] : $data['Product_Name'])
                    ),
                );
                if($skuCount == 0){
                    $this->model->insertCsvData($data , $table);
                    array_push($insert,$details);
                }
                else{
                    $this->model->updateCsvData($data , $table, $details['sku']);
                    array_push($update,$details);
                }
            }else{
                $errors[$rowCount] = $validator->errors();
            }
            $rowCount++;
        }
        $this->data['insertion'] = $insert;
        $this->data['updation'] = $update;
        $this->data['pageNote'] = 'Summary';
        $this->data['status'] = 'Success';
        $this->data['successMessage'] = 'Csv Imported Successfully !';
        $this->data['errors'] = $errors;
        return view('sximo.importReport.import_summary',$this->data);
    }

    public function get_csvFields_mapping_data($table, $marketplace)
    {
        // return \DB::select("SELECT * FROM csv_fields_mapping WHERE `table` = '".$table."' AND marketplace_id = $marketplace ");
        return \DB::select("SELECT * FROM csv_fields_mapping WHERE `table` = '".$table."' AND FIND_IN_SET($marketplace,marketplace_id) > 0 ");
    }

    public function replace_fields_space_with_underScore($fields)
    {
        foreach($fields as $f)
            $csv_file_fields[] = preg_replace('/[\s\-]+/', '_', $f);
        
        return $csv_file_fields;
    }

    public function getFileData($filename)
    {
        $file = fopen('./uploads/'.$filename,"r");
        while (($data = fgetcsv($file)) !== FALSE)
        {
            $csv[] = $data;
            
        }
        fclose($file);
        return $csv;
    }

    public function get_fields_to_import($data, $is_not_import)
    {
        $fields = array();
        foreach($data as $f){
            if(isset($is_not_import))
            {
                if(!array_key_exists($f->table_field, $is_not_import)){
                    $fields[] = $f->table_field;
                }
            }else{
                $fields[] = $f->table_field;
            }
        }
        return $fields;
    }

    public function get_validation_rules($data)
    {
        foreach($data as $row){
            if($row->validation_rules != ""){
                $rules[$row->table_field] = $row->validation_rules;
            }
        }
        return $rules;
    }

    public function validateData($data, $rules)
    {
        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public function validateSku($table,$sku,$marketplace)
    {
        \DB::enableQueryLog();
        $query = \DB::select("SELECT * FROM $table WHERE sku = '".$sku."' AND marketplace_id = $marketplace");
        $quries = \DB::getQueryLog();
        //dd($quries);
        return count($query);
    }

    public function convert_row_to_data_obj($value)
    {
        return (isset($value) ? $value : '' );
    }
}