@extends('layouts.app')

@section('content')

<div class="page-titles">
  <h2> {{ $pageTitle }}  <small>  {{ $pageNote }} </small> </h2>
</div>

{!! Form::open(array('url'=>'sximo/import_process', 'class'=>'form-horizontal validated', 'files' => true , 'parsley-validate'=>'','novalidate'=>'')) !!}

<div class="toolbar-nav">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-7">
            <strong>Database Column Mapping with CSV File Fields</strong>
        </div>
        <div class="col-md-3 text-right">
            <div class="btn-group">
                <button type="submit" class="btn btn-primary btn-sm  " style="text-transform: uppercase;"> Import Report </button>
            </div>
        </div>

    </div>            
</div>

<center>
<table class="table table-hover " id="table" style="width:90% !important;">
    <thead class="no-border">	
        <tr>
            <th class="thead" field="name1">No.</th>
            <th class="thead" field="name2">Database Columns</th>
            <th class="thead" field="name3">CSV File Colummns</th>
            <th class="thead" field="name4">Do Not Import!</th>
        </tr>
    </thead>

    <tbody class="no-border-x no-border-y ui-sortable">	
        <input type="hidden" value="<?php echo $table; ?>" name="table" />
        <input type="hidden" value="<?php echo $marketplace; ?>" name="marketplace" />
        <input type="hidden" value="<?php echo $fileName; ?>" name="fileName" />
        <?php 
            $num = 1; 
            foreach($data as $f){
        ?>
        <tr>
            <td><?php echo $num; ?></td>
            <td><?php echo $f->table_field; ?></td>
            <td><?php echo $f->csv_field; ?></td>
            <td>
                <?php if($f->table_field == "id" || $f->table_field == "sku") { ?>
                    <input disabled name="is_not_import[<?php echo $f->table_field;?>]" class="c1 filled-in" type="checkbox" value="1" id="is_not_import-<?php echo $f->table_field;?>" data-parsley-multiple="is_not_import<?php echo $f->table_field;?>">
                <?php } else {?>
                    <input name="is_not_import[<?php echo $f->table_field;?>]" class="c1 filled-in" type="checkbox" value="1" id="is_not_import-<?php echo $f->table_field;?>" data-parsley-multiple="is_not_import<?php echo $f->table_field;?>">
                <?php } ?>
                    <label for="is_not_import-<?php echo $f->table_field;?>" style="color:red;">Do Not Import!</label>
            </td>
        </tr>
        <?php
            $num++;
            }
        ?>
    </tbody>
</table>
</center>

@stop