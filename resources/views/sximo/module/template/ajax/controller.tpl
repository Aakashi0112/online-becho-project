<?php namespace App\Http\Controllers;

use App\Models\{controller};
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 

class {controller}Controller extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = '{class}';
	static $per_page	= '10';
	
	public function __construct() 
	{
		parent::__construct();
		$this->model = new {controller}();
		{masterdetailmodel}
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = array();
	
		$this->data = array(
			'pageTitle'			=> 	$this->info['title'],
			'pageNote'			=>  $this->info['note'],
			'pageModule'		=> '{class}',
			'pageUrl'			=>  url('{class}'),
			'return' 			=> 	self::returnUrl()	
		);		
		{masterdetailinfo}		
	} 
	
	public function index()
	{
		if(!\Auth::check()) 
			return redirect('user/login')->with('msgstatus', 'error')->with('messagetext','You are not login');

		\AjaxHelpers::test('index',$this->module,0);
		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));
		if($this->access['is_view'] ==0) 
			return redirect('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
				
		$this->data['access']		= $this->access;	
		\AjaxHelpers::test('index',$this->module,1,null,null,$this->data);		
		return view($this->module.'.index',$this->data);
	}
	function create( Request $request ) 
	{
		\AjaxHelpers::test('create',$this->module,0, $request);
		$id = 0;
		$this->hook( $request  );
		if($this->access['is_add'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

		$this->data['row'] = $this->model->getColumnTable( $this->info['table']); 
		{masterdetailsubform}
		$this->data['id'] = '';
		$this->data['setting'] 		= $this->info['setting']; 
		\AjaxHelpers::test('create',$this->module,1, $request,null,$this->data);
		return view($this->module.'.form',$this->data);
	}		
	function edit( Request $request , $id ) 
	{
		\AjaxHelpers::test('edit',$this->module,0,$request, $id);
		$this->hook( $request , $id );
		if(!isset($this->data['row']))
			return redirect($this->module)->with('message','Record Not Found !')->with('status','error');
		if($this->access['is_edit'] ==0 )
			return redirect('dashboard')->with('message',__('core.note_restric'))->with('status','error');

		$this->data['row'] = (array) $this->data['row'];
		{masterdetailsubform}
		$this->data['id'] = $id;
		\AjaxHelpers::test('edit',$this->module,1,$request, $id,$this->data);
		return view($this->module.'.form',$this->data);
	}
	function show( Request $request , $id ) 
	{
		/* Handle import , export and view */
		\AjaxHelpers::test('show',$this->module,0, $request, $id);
		$task =$id ;
		switch( $task)
		{
			case 'data':
				$this->grab( $request) ;
				\AjaxHelpers::test('show',$this->module,1, $request, $id, $this->data);
				return view( $this->module.'.table',$this->data);
				break;
			case 'search':
				\AjaxHelpers::test('show',$this->module,1, $request, $id);
				return $this->getSearch();	
				break;
							
			case 'lookup':
				\AjaxHelpers::test('show',$this->module,1, $request, $id);
				return $this->getLookup($request );
				break;				

			case 'comboselect':
				\AjaxHelpers::test('show',$this->module,1, $request, $id);
				return $this->getComboselect( $request );
				break;
			case 'import':
				\AjaxHelpers::test('show',$this->module,1, $request, $id);
				return $this->getImport( $request );
				break;
			case 'export':
				\AjaxHelpers::test('show',$this->module,1, $request, $id);
				return $this->getExport( $request );
				break;
			default:
				$this->hook( $request , $id );
				if(!isset($this->data['row']))
					return redirect($this->module)->with('message','Record Not Found !')->with('status','error');

				if($this->access['is_detail'] ==0) 
					return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

				\AjaxHelpers::test('show',$this->module,1, $request, $id, $this->data);
				return view($this->module.'.view',$this->data);	
				break;		
		}
	}	

	function store( Request $request  )
	{
		\AjaxHelpers::test('store',$this->module,0, $request);
		$task = $request->input('action_task');
		switch ($task)
		{
			default:
				$rules = $this->validateForm();
				$validator = Validator::make($request->all(), $rules);
				if ($validator->passes()) 
				{
					$data = $this->validatePost( $request );
					$id = $this->model->insertRow($data , $request->input( $this->info['key']));
					{masterdetailsave}
					/* Insert logs */
					$this->model->logs($request , $id);
					
					\AjaxHelpers::test('store',$this->module,1, $request,$task);
					return response()->json(array(
						'status'=>'success',
						'message'=> __('core.note_success')
						));	
					
				} else {

					$message = $this->validateListError(  $validator->getMessageBag()->toArray() );
					\AjaxHelpers::test('store',$this->module,1, $request,$task);
					return response()->json(array(
						'message'	=> $message,
						'status'	=> 'error'
					));	
				}
				break;
			case 'delete':
				$result = $this->destroy( $request );
				\AjaxHelpers::test('store',$this->module,1, $request,$task,$result);
				return response()->json($result);
				break;

			case 'import':
				\AjaxHelpers::test('store',$this->module,1, $request,$task);
				return $this->PostImport( $request );
				break;

			case 'copy':
				$result = $this->copy( $request );
				\AjaxHelpers::test('store',$this->module,1, $request,$task,$result);
				return response()->json($result);
				break;
			case 'updatestatus':
					$data = $this->validatePost( $request );
					$id = $this->model->insertRow($data , $request->input( $this->info['key']));
					$this->model->logs($request , $id);
					
					\AjaxHelpers::test('store',$this->module,1, $request,$task);
					return response()->json(array(
						'status'=>'success',
						'message'=> __('core.note_success')
						));	
			break;
						
		}	
	
	}	


	public function destroy( $request)
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');

		\AjaxHelpers::test('destroy',$this->module,0, $request);
		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));
		if($this->access['is_remove'] ==0) 
			return redirect('dashboard')
				->with('message', __('core.note_restric'))->with('status','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			$this->model->deleteDataRow( $this->info['table'],$this->info['key'],$request->input('ids'));
		//	{masterdetaildelete}
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect

			\AjaxHelpers::test('destroy',$this->module,1, $request);
        	return ['message'=>__('core.note_success_delete'),'status'=>'success'];	
	
		} else {
			\AjaxHelpers::test('destroy',$this->module,1, $request);
			return ['message'=>__('No Item Deleted'),'status'=>'error'];				
		}

	}		

}