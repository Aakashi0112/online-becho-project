

		 {!! Form::open(array('url'=>'inventory/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Inventory</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Snapshot-date" class=" control-label col-md-4 "> Snapshot-date </label>
										<div class="col-md-8">
										  <input  type='text' name='snapshot-date' id='snapshot-date' value='{{ $row['snapshot-date'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sku" class=" control-label col-md-4 "> Sku </label>
										<div class="col-md-8">
										  <input  type='text' name='sku' id='sku' value='{{ $row['sku'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Fnsku" class=" control-label col-md-4 "> Fnsku </label>
										<div class="col-md-8">
										  <input  type='text' name='fnsku' id='fnsku' value='{{ $row['fnsku'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Asin" class=" control-label col-md-4 "> Asin </label>
										<div class="col-md-8">
										  <input  type='text' name='asin' id='asin' value='{{ $row['asin'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Product-name" class=" control-label col-md-4 "> Product-name </label>
										<div class="col-md-8">
										  <input  type='text' name='product-name' id='product-name' value='{{ $row['product-name'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Condition" class=" control-label col-md-4 "> Condition </label>
										<div class="col-md-8">
										  <input  type='text' name='condition' id='condition' value='{{ $row['condition'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Available" class=" control-label col-md-4 "> Available </label>
										<div class="col-md-8">
										  <input  type='text' name='available' id='available' value='{{ $row['available'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Pending-removal-quantity" class=" control-label col-md-4 "> Pending-removal-quantity </label>
										<div class="col-md-8">
										  <input  type='text' name='pending-removal-quantity' id='pending-removal-quantity' value='{{ $row['pending-removal-quantity'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-0-to-90-days" class=" control-label col-md-4 "> Inv-age-0-to-90-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-0-to-90-days' rows='5' id='inv-age-0-to-90-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-0-to-90-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-91-to-180-days" class=" control-label col-md-4 "> Inv-age-91-to-180-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-91-to-180-days' rows='5' id='inv-age-91-to-180-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-91-to-180-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-181-to-270-days" class=" control-label col-md-4 "> Inv-age-181-to-270-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-181-to-270-days' rows='5' id='inv-age-181-to-270-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-181-to-270-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-271-to-365-days" class=" control-label col-md-4 "> Inv-age-271-to-365-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-271-to-365-days' rows='5' id='inv-age-271-to-365-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-271-to-365-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-365-plus-days" class=" control-label col-md-4 "> Inv-age-365-plus-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-365-plus-days' rows='5' id='inv-age-365-plus-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-365-plus-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Currency" class=" control-label col-md-4 "> Currency </label>
										<div class="col-md-8">
										  <textarea name='currency' rows='5' id='currency' class='form-control form-control-sm '  
				           >{{ $row['currency'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Qty-to-be-charged-ltsf-5-mo" class=" control-label col-md-4 "> Qty-to-be-charged-ltsf-5-mo </label>
										<div class="col-md-8">
										  <textarea name='qty-to-be-charged-ltsf-5-mo' rows='5' id='qty-to-be-charged-ltsf-5-mo' class='form-control form-control-sm '  
				           >{{ $row['qty-to-be-charged-ltsf-5-mo'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Projected-ltsf-5-mo" class=" control-label col-md-4 "> Projected-ltsf-5-mo </label>
										<div class="col-md-8">
										  <textarea name='projected-ltsf-5-mo' rows='5' id='projected-ltsf-5-mo' class='form-control form-control-sm '  
				           >{{ $row['projected-ltsf-5-mo'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Qty-to-be-charged-ltsf-12-mo" class=" control-label col-md-4 "> Qty-to-be-charged-ltsf-12-mo </label>
										<div class="col-md-8">
										  <textarea name='qty-to-be-charged-ltsf-12-mo' rows='5' id='qty-to-be-charged-ltsf-12-mo' class='form-control form-control-sm '  
				           >{{ $row['qty-to-be-charged-ltsf-12-mo'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Estimated-ltsf-next-charge" class=" control-label col-md-4 "> Estimated-ltsf-next-charge </label>
										<div class="col-md-8">
										  <textarea name='estimated-ltsf-next-charge' rows='5' id='estimated-ltsf-next-charge' class='form-control form-control-sm '  
				           >{{ $row['estimated-ltsf-next-charge'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Units-shipped-t7" class=" control-label col-md-4 "> Units-shipped-t7 </label>
										<div class="col-md-8">
										  <textarea name='units-shipped-t7' rows='5' id='units-shipped-t7' class='form-control form-control-sm '  
				           >{{ $row['units-shipped-t7'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Units-shipped-t30" class=" control-label col-md-4 "> Units-shipped-t30 </label>
										<div class="col-md-8">
										  <textarea name='units-shipped-t30' rows='5' id='units-shipped-t30' class='form-control form-control-sm '  
				           >{{ $row['units-shipped-t30'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Units-shipped-t60" class=" control-label col-md-4 "> Units-shipped-t60 </label>
										<div class="col-md-8">
										  <textarea name='units-shipped-t60' rows='5' id='units-shipped-t60' class='form-control form-control-sm '  
				           >{{ $row['units-shipped-t60'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Units-shipped-t90" class=" control-label col-md-4 "> Units-shipped-t90 </label>
										<div class="col-md-8">
										  <textarea name='units-shipped-t90' rows='5' id='units-shipped-t90' class='form-control form-control-sm '  
				           >{{ $row['units-shipped-t90'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Alert" class=" control-label col-md-4 "> Alert </label>
										<div class="col-md-8">
										  <textarea name='alert' rows='5' id='alert' class='form-control form-control-sm '  
				           >{{ $row['alert'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Your-price" class=" control-label col-md-4 "> Your-price </label>
										<div class="col-md-8">
										  <textarea name='your-price' rows='5' id='your-price' class='form-control form-control-sm '  
				           >{{ $row['your-price'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sales-price" class=" control-label col-md-4 "> Sales-price </label>
										<div class="col-md-8">
										  <textarea name='sales-price' rows='5' id='sales-price' class='form-control form-control-sm '  
				           >{{ $row['sales-price'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Lowest-price-new-plus-shipping" class=" control-label col-md-4 "> Lowest-price-new-plus-shipping </label>
										<div class="col-md-8">
										  <textarea name='lowest-price-new-plus-shipping' rows='5' id='lowest-price-new-plus-shipping' class='form-control form-control-sm '  
				           >{{ $row['lowest-price-new-plus-shipping'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Lowest-price-used" class=" control-label col-md-4 "> Lowest-price-used </label>
										<div class="col-md-8">
										  <textarea name='lowest-price-used' rows='5' id='lowest-price-used' class='form-control form-control-sm '  
				           >{{ $row['lowest-price-used'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Recommended-action" class=" control-label col-md-4 "> Recommended-action </label>
										<div class="col-md-8">
										  <textarea name='recommended-action' rows='5' id='recommended-action' class='form-control form-control-sm '  
				           >{{ $row['recommended-action'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Healthy-inventory-level" class=" control-label col-md-4 "> Healthy-inventory-level </label>
										<div class="col-md-8">
										  <textarea name='healthy-inventory-level' rows='5' id='healthy-inventory-level' class='form-control form-control-sm '  
				           >{{ $row['healthy-inventory-level'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Recommended-sales-price" class=" control-label col-md-4 "> Recommended-sales-price </label>
										<div class="col-md-8">
										  <textarea name='recommended-sales-price' rows='5' id='recommended-sales-price' class='form-control form-control-sm '  
				           >{{ $row['recommended-sales-price'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Recommended-sale-duration-days" class=" control-label col-md-4 "> Recommended-sale-duration-days </label>
										<div class="col-md-8">
										  <textarea name='recommended-sale-duration-days' rows='5' id='recommended-sale-duration-days' class='form-control form-control-sm '  
				           >{{ $row['recommended-sale-duration-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Recommended-removal-quantity" class=" control-label col-md-4 "> Recommended-removal-quantity </label>
										<div class="col-md-8">
										  <textarea name='recommended-removal-quantity' rows='5' id='recommended-removal-quantity' class='form-control form-control-sm '  
				           >{{ $row['recommended-removal-quantity'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Estimated-cost-savings-of-recommended-actions" class=" control-label col-md-4 "> Estimated-cost-savings-of-recommended-actions </label>
										<div class="col-md-8">
										  <textarea name='estimated-cost-savings-of-recommended-actions' rows='5' id='estimated-cost-savings-of-recommended-actions' class='form-control form-control-sm '  
				           >{{ $row['estimated-cost-savings-of-recommended-actions'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sell-through" class=" control-label col-md-4 "> Sell-through </label>
										<div class="col-md-8">
										  <textarea name='sell-through' rows='5' id='sell-through' class='form-control form-control-sm '  
				           >{{ $row['sell-through'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Item-volume" class=" control-label col-md-4 "> Item-volume </label>
										<div class="col-md-8">
										  <textarea name='item-volume' rows='5' id='item-volume' class='form-control form-control-sm '  
				           >{{ $row['item-volume'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Volume-unit-measurement" class=" control-label col-md-4 "> Volume-unit-measurement </label>
										<div class="col-md-8">
										  <textarea name='volume-unit-measurement' rows='5' id='volume-unit-measurement' class='form-control form-control-sm '  
				           >{{ $row['volume-unit-measurement'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Storage-type" class=" control-label col-md-4 "> Storage-type </label>
										<div class="col-md-8">
										  <textarea name='storage-type' rows='5' id='storage-type' class='form-control form-control-sm '  
				           >{{ $row['storage-type'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Storage-volume" class=" control-label col-md-4 "> Storage-volume </label>
										<div class="col-md-8">
										  <textarea name='storage-volume' rows='5' id='storage-volume' class='form-control form-control-sm '  
				           >{{ $row['storage-volume'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Marketplace" class=" control-label col-md-4 "> Marketplace </label>
										<div class="col-md-8">
										  <textarea name='marketplace' rows='5' id='marketplace' class='form-control form-control-sm '  
				           >{{ $row['marketplace'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Product-group" class=" control-label col-md-4 "> Product-group </label>
										<div class="col-md-8">
										  <textarea name='product-group' rows='5' id='product-group' class='form-control form-control-sm '  
				           >{{ $row['product-group'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sales-rank" class=" control-label col-md-4 "> Sales-rank </label>
										<div class="col-md-8">
										  <textarea name='sales-rank' rows='5' id='sales-rank' class='form-control form-control-sm '  
				           >{{ $row['sales-rank'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Days-of-supply" class=" control-label col-md-4 "> Days-of-supply </label>
										<div class="col-md-8">
										  <textarea name='days-of-supply' rows='5' id='days-of-supply' class='form-control form-control-sm '  
				           >{{ $row['days-of-supply'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Estimated-excess-quantity" class=" control-label col-md-4 "> Estimated-excess-quantity </label>
										<div class="col-md-8">
										  <textarea name='estimated-excess-quantity' rows='5' id='estimated-excess-quantity' class='form-control form-control-sm '  
				           >{{ $row['estimated-excess-quantity'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Weeks-of-cover-t30" class=" control-label col-md-4 "> Weeks-of-cover-t30 </label>
										<div class="col-md-8">
										  <textarea name='weeks-of-cover-t30' rows='5' id='weeks-of-cover-t30' class='form-control form-control-sm '  
				           >{{ $row['weeks-of-cover-t30'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Weeks-of-cover-t90" class=" control-label col-md-4 "> Weeks-of-cover-t90 </label>
										<div class="col-md-8">
										  <textarea name='weeks-of-cover-t90' rows='5' id='weeks-of-cover-t90' class='form-control form-control-sm '  
				           >{{ $row['weeks-of-cover-t90'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Featuredoffer-price" class=" control-label col-md-4 "> Featuredoffer-price </label>
										<div class="col-md-8">
										  <textarea name='featuredoffer-price' rows='5' id='featuredoffer-price' class='form-control form-control-sm '  
				           >{{ $row['featuredoffer-price'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sales-shipped-last-7-days" class=" control-label col-md-4 "> Sales-shipped-last-7-days </label>
										<div class="col-md-8">
										  <textarea name='sales-shipped-last-7-days' rows='5' id='sales-shipped-last-7-days' class='form-control form-control-sm '  
				           >{{ $row['sales-shipped-last-7-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sales-shipped-last-30-days" class=" control-label col-md-4 "> Sales-shipped-last-30-days </label>
										<div class="col-md-8">
										  <textarea name='sales-shipped-last-30-days' rows='5' id='sales-shipped-last-30-days' class='form-control form-control-sm '  
				           >{{ $row['sales-shipped-last-30-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sales-shipped-last-60-days" class=" control-label col-md-4 "> Sales-shipped-last-60-days </label>
										<div class="col-md-8">
										  <textarea name='sales-shipped-last-60-days' rows='5' id='sales-shipped-last-60-days' class='form-control form-control-sm '  
				           >{{ $row['sales-shipped-last-60-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sales-shipped-last-90-days" class=" control-label col-md-4 "> Sales-shipped-last-90-days </label>
										<div class="col-md-8">
										  <textarea name='sales-shipped-last-90-days' rows='5' id='sales-shipped-last-90-days' class='form-control form-control-sm '  
				           >{{ $row['sales-shipped-last-90-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-0-to-30-days" class=" control-label col-md-4 "> Inv-age-0-to-30-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-0-to-30-days' rows='5' id='inv-age-0-to-30-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-0-to-30-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-31-to-60-days" class=" control-label col-md-4 "> Inv-age-31-to-60-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-31-to-60-days' rows='5' id='inv-age-31-to-60-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-31-to-60-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-61-to-90-days" class=" control-label col-md-4 "> Inv-age-61-to-90-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-61-to-90-days' rows='5' id='inv-age-61-to-90-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-61-to-90-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-91-to-150-days" class=" control-label col-md-4 "> Inv-age-91-to-150-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-91-to-150-days' rows='5' id='inv-age-91-to-150-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-91-to-150-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-151-to-180-days" class=" control-label col-md-4 "> Inv-age-151-to-180-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-151-to-180-days' rows='5' id='inv-age-151-to-180-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-151-to-180-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-181-to-330-days" class=" control-label col-md-4 "> Inv-age-181-to-330-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-181-to-330-days' rows='5' id='inv-age-181-to-330-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-181-to-330-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inv-age-331-to-365-days" class=" control-label col-md-4 "> Inv-age-331-to-365-days </label>
										<div class="col-md-8">
										  <textarea name='inv-age-331-to-365-days' rows='5' id='inv-age-331-to-365-days' class='form-control form-control-sm '  
				           >{{ $row['inv-age-331-to-365-days'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Estimated-storage-cost-next-month" class=" control-label col-md-4 "> Estimated-storage-cost-next-month </label>
										<div class="col-md-8">
										  <textarea name='estimated-storage-cost-next-month' rows='5' id='estimated-storage-cost-next-month' class='form-control form-control-sm '  
				           >{{ $row['estimated-storage-cost-next-month'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inbound-quantity" class=" control-label col-md-4 "> Inbound-quantity </label>
										<div class="col-md-8">
										  <textarea name='inbound-quantity' rows='5' id='inbound-quantity' class='form-control form-control-sm '  
				           >{{ $row['inbound-quantity'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inbound-working" class=" control-label col-md-4 "> Inbound-working </label>
										<div class="col-md-8">
										  <textarea name='inbound-working' rows='5' id='inbound-working' class='form-control form-control-sm '  
				           >{{ $row['inbound-working'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inbound-shipped" class=" control-label col-md-4 "> Inbound-shipped </label>
										<div class="col-md-8">
										  <textarea name='inbound-shipped' rows='5' id='inbound-shipped' class='form-control form-control-sm '  
				           >{{ $row['inbound-shipped'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Inbound-received" class=" control-label col-md-4 "> Inbound-received </label>
										<div class="col-md-8">
										  <textarea name='inbound-received' rows='5' id='inbound-received' class='form-control form-control-sm '  
				           >{{ $row['inbound-received'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="No-sale-last-6-months" class=" control-label col-md-4 "> No-sale-last-6-months </label>
										<div class="col-md-8">
										  <textarea name='no-sale-last-6-months' rows='5' id='no-sale-last-6-months' class='form-control form-control-sm '  
				           >{{ $row['no-sale-last-6-months'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Reserved-quantity" class=" control-label col-md-4 "> Reserved-quantity </label>
										<div class="col-md-8">
										  <textarea name='reserved-quantity' rows='5' id='reserved-quantity' class='form-control form-control-sm '  
				           >{{ $row['reserved-quantity'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Unfulfillable-quantity" class=" control-label col-md-4 "> Unfulfillable-quantity </label>
										<div class="col-md-8">
										  <textarea name='unfulfillable-quantity' rows='5' id='unfulfillable-quantity' class='form-control form-control-sm '  
				           >{{ $row['unfulfillable-quantity'] }}</textarea> 
										 </div> 
										 
									  </div> </fieldset></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
