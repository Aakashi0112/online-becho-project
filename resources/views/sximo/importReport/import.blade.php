@extends('layouts.app')

@section('content')

<div class="page-titles">
  <h2> {{ $pageTitle }}  <small>  {{ $pageNote }} </small> </h2>
</div>


<!-- {!! Form::open(array('url'=>'sximo/import/report/', 'class'=>'form-horizontal validated', 'files' => true , 'parsley-validate'=>'','novalidate'=>'')) !!} -->
{!! Form::open(array('url'=>'sximo/import_parse', 'class'=>'form-horizontal validated', 'files' => true , 'parsley-validate'=>'','novalidate'=>'')) !!}
<div class="row">
	<div class="col-md-12">

		<div class="form-group row">
			<label class="col-sm-3 text-right"></label>
			<div class="col-sm-9">	
		
				<ul class="parsley-error-list">
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				</ul> 
			
			</div>	  
		</div>
		<div class="form-group row has-feedback">
			<label class="col-sm-3 text-right"> Select MarketPlace </label>
				<div class="col-sm-6">	
                    <select name='marketplace' rows='5' id='marketplace'  style="width:100%" class='select2'>
                        <option value=""> -- Select MarketPlace or Platform -- </option>
                        <optgroup label="Marketplace ">
                     
                        <?php
                            foreach($marketplaces as $marketplace)
                            {
                                foreach($marketplace as $key => $val){
                                    echo "<option value='$val->id'>$val->name</option>";
                                }
                            }
                        ?>
                        </optgroup>					
                    </select> 
                    <!-- @error('marketplace')
                        <div class="parsley-error-list" style="color:white">{{ $message }}</div>
                    @enderror -->
				</div>
		</div>		
		
		<div class="form-group row ">
			<label class="col-sm-3 text-right"> Select Report type </label>
			<div class="col-sm-6">	
                <select name='report' rows='5' id='report'  style="width:100%" class='select2'>
                    <option value=""> -- Select Report type -- </option>
                    <option value="inventory">Inventory</option>
                    <option value="order">Order</option>
                    <option value="payment">Payment</option>
                    <option value="article_master">Article Master</option>
                </select> 
			</div>
		</div>

        <div class="form-group row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6">
                <div class="fileUpload btn " > 
                    <span>  <i class="fa fa-copy"></i>  </span>
                    <div class="title">  Upload CSV File </div>
                    <input type="file" name="fileimport" class="upload" id="fileimport" />
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-3 text-right">
                
            </div>	
            <div class="col-sm-9">
                    <p><strong>Note:</strong> Please Make sure your CSV file format field column same as database fields.
            </div>            
        </div>

		<div class="form-group row">
			<label class="col-sm-3">&nbsp;</label>

            <div class="col-sm-2">	
				<button type="submit" class="btn btn-primary btn-sm  " style="text-transform: uppercase;"> Parse CSV </button>
			</div>	

		</div>

	</div>
</div>

<script>
    $("#mapping").click(function (e) {
        e.preventDefault();
        //debugger;
        var marketplace = $('#marketplace').val();
        var reportType = $('#report').val();
        //var csv = document.querySelector("#fileimport");
        var csv = document.getElementById('fileimport').files[0];
        alert('csv file: ' + csv);
        
        //alert(marketplace + reportType);

        $.ajax( {
            url: 'http://localhost/online-becho/public/sximo/import/mapping',
            //url: '../resources/views/sximo/reports/mapping',
            data: {
                marketplace: marketplace,
                reportType: reportType,
                csv: csv
            },
            dataType: 'json',
            type: 'post',
            success: function (  ) {
                // phpTab.css( 'display', 'block' );
                // $('div.tabs div.php').append(
                //     '<code class="multiline language-php">'+txt+'</code>'
                // );
                // SyntaxHighlighter.highlight( {}, $('div.tabs div.php code')[0] );
                alert('ajax call successfully run!!');
            }
        } );
	});
</script>

@stop