@if($setting['form-method'] =='native')
<div class="card">
	<div class="card-body">
@endif
		<div class="form-ajax-box">
		{!! Form::open(array('url'=>'companies?return='.$return, 'class'=>'form-horizontal  sximo-form validated','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'companiesFormAjax')) !!}

		<div class="toolbar-nav">	
			<div class="row">	
					
				<div class="col-md-6 ">
					<a href="javascript://ajax" onclick="ajaxViewClose('#{{ $pageModule }}')" class="tips btn btn-sm  btn-danger" title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a>			
				</div>
				<div class="col-sm-6 text-right">	
					<div class="btn-group">
						<button type="submit" class="btn btn-sm btn-primary  " name="apply">{{ Lang::get('core.sb_apply') }} </button>
						<button type="submit" class="btn btn-sm btn-success " name="save">  {{ Lang::get('core.sb_save') }} </button>
					</div>	
				</div>
						
			</div>
		</div>	


		<div class="col-md-12">
						<fieldset><legend> companies</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Name" class=" control-label col-md-4 "> Name </label>
										<div class="col-md-8">
										  <input  type='text' name='name' id='name' value='{{ $row['name'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Address" class=" control-label col-md-4 "> Address </label>
										<div class="col-md-8">
										  <textarea name='address' rows='5' id='address' class='form-control form-control-sm '  
				           >{{ $row['address'] }}</textarea> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="City" class=" control-label col-md-4 "> City </label>
										<div class="col-md-8">
										  <input  type='text' name='city' id='city' value='{{ $row['city'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Zip code" class=" control-label col-md-4 "> Zip code </label>
										<div class="col-md-8">
										  <input  type='text' name='zip' id='zip' value='{{ $row['zip'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Country" class=" control-label col-md-4 "> Country </label>
										<div class="col-md-8">
										  <input  type='text' name='country' id='country' value='{{ $row['country'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Contact Person" class=" control-label col-md-4 "> Contact Person </label>
										<div class="col-md-8">
										  <input  type='text' name='contact_person' id='contact_person' value='{{ $row['contact_person'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Contact Number" class=" control-label col-md-4 "> Contact Number </label>
										<div class="col-md-8">
										  <input  type='text' name='contact_number' id='contact_number' value='{{ $row['contact_number'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Email Address" class=" control-label col-md-4 "> Email Address </label>
										<div class="col-md-8">
										  <input  type='text' name='email' id='email' value='{{ $row['email'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Marketplace Id" class=" control-label col-md-4 "> Marketplace Id </label>
										<div class="col-md-8">
										  <select name='marketplace_id[]' multiple rows='5' id='marketplace_id' class='select2 '   ></select> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Url" class=" control-label col-md-4 "> Url </label>
										<div class="col-md-8">
										  <input  type='text' name='url' id='url' value='{{ $row['url'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Staus" class=" control-label col-md-4 "> Staus </label>
										<div class="col-md-8">
										  
					<?php $staus = explode(',',$row['staus']);
					$staus_opt = array( 'Active' => 'Active' ,  'Inactive' => 'Inactive' , ); ?>
					<select name='staus' rows='5'   class='select2 '  > 
						<?php 
						foreach($staus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['staus'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 
									  </div> {!! Form::hidden('modified', $row['modified']) !!}</fieldset></div>									
							
		<input type="hidden" name="action_task" value="save" />

		{!! Form::close() !!}
		</div>
@if($setting['form-method'] =='native')

	</div>
</div>
@endif
@include('sximo.module.template.ajax.formjavascript')


<script type="text/javascript">
$(document).ready(function() { 
	 
	
	
		$("#marketplace_id").jCombo("{!! url('companies/comboselect?filter=marketplace:id:name') !!}",
		{  selected_value : '{{ $row["marketplace_id"] }}' });
		 	
	 
	
	var form = $('#companiesFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley().isValid()){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  function() {
				},
				success		:   function(data) {

					if(data.status == 'success')
					{
						ajaxViewClose('#{{ $pageModule }}');
						ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
						notyMessage(data.message);	
						$('#sximo-modal').modal('hide');	
					} else {
						notyMessageError(data.message);	
						return false;
					}	

				}  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

</script>		 