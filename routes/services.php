<?php
        
// Start Routes for fullcrud 
Route::resource('services/fullcrud','Services\FullcrudController');
// End Routes for fullcrud 

                    
// Start Routes for formdefault 
Route::resource('services/formdefault','Services\FormdefaultController');
// End Routes for formdefault 

                    
// Start Routes for formgroupped 
Route::resource('services/formgroupped','Services\FormgrouppedController');
// End Routes for formgroupped 

                    
// Start Routes for formwizard 
Route::resource('services/formwizard','Services\FormwizardController');
// End Routes for formwizard 

                    
// Start Routes for formtab 
Route::resource('services/formtab','Services\FormtabController');
// End Routes for formtab 

                    
// Start Routes for sxtask 
Route::resource('services/sxtask','Services\SxtaskController');
// End Routes for sxtask 

                    
// Start Routes for classgroup 
Route::resource('services/classgroup','Services\ClassgroupController');
// End Routes for classgroup 

                    
// Start Routes for classgroup 
Route::resource('services/classgroup','Services\ClassgroupController');
// End Routes for classgroup 

                    
// Start Routes for userdatatestcontroller 
Route::resource('services/userdatatestcontroller','Services\UserdatatestcontrollerController');
// End Routes for userdatatestcontroller 

                    
// Start Routes for userdatatesttwo 
Route::resource('services/userdatatesttwo','Services\UserdatatesttwoController');
// End Routes for userdatatesttwo 

                    
// Start Routes for userdatatestthree 
Route::resource('services/userdatatestthree','Services\UserdatatestthreeController');
// End Routes for userdatatestthree 

                    
// Start Routes for companies 
Route::resource('services/companies','Services\CompaniesController');
// End Routes for companies 

                    
// Start Routes for systemusers 
Route::resource('services/systemusers','Services\SystemusersController');
// End Routes for systemusers 

                    
// Start Routes for marketplace 
Route::resource('services/marketplace','Services\MarketplaceController');
// End Routes for marketplace 

                    
// Start Routes for inventory 
Route::resource('services/inventory','Services\InventoryController');
// End Routes for inventory 

                    
// Start Routes for events 
Route::resource('services/events','Services\EventsController');
// End Routes for events 

                    ?>